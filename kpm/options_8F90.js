var options_8F90 =
[
    [ "optread", "options_8F90.html#a431b88966a7e9151f5d19a55cf8e7cf2", null ],
    [ "delta", "options_8F90.html#af00a38abdfd6a4e5a77837793771c687", null ],
    [ "dstart", "options_8F90.html#a5d69957c86c0df312276583a3932d2f5", null ],
    [ "dw", "options_8F90.html#a12d0a1a9ef81defb54b0287ac1099964", null ],
    [ "energymax", "options_8F90.html#ae4b6ab8de5ae9c23391aa886094f4657", null ],
    [ "energymin", "options_8F90.html#a124e28bce2f5bab8e5cf5f3ea2a7bfe7", null ],
    [ "kerlabel", "options_8F90.html#ad1d8be642e3c516e4eb91f14a48f4882", null ],
    [ "label_len", "options_8F90.html#a2466dc5911e6fd378e516258a4141f18", null ],
    [ "lattorder", "options_8F90.html#ad03f6634fd2e2dea42b0541e26a3266d", null ],
    [ "lresol", "options_8F90.html#aa7154264d7830834542fc1f75f1d6355", null ],
    [ "memory", "options_8F90.html#ac063165b621fc93b4ef457b39eb6691f", null ],
    [ "ngrid", "options_8F90.html#a0ae666e41c99328917e470013ecdb131", null ],
    [ "nsteps", "options_8F90.html#a770889d42f85f6ccbea6797fd638fba7", null ],
    [ "numthreads", "options_8F90.html#aed31701f0c6077da412dfdf1bcdc7f7a", null ],
    [ "poldegree", "options_8F90.html#ad924cd9cd7430fa0f17353ae821f2f4c", null ],
    [ "slabel", "options_8F90.html#a5dbebbab4c4ae1b4ee93facde28d0087", null ],
    [ "thop", "options_8F90.html#a62ff0315a76e3d123c41f412e61088a9", null ]
];