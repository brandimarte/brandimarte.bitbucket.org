var NAVTREE =
[
  [ "KPM", "index.html", [
    [ "Modules", null, [
      [ "Modules List", "namespaces.html", "namespaces" ],
      [ "Module Members", "namespacemembers.html", [
        [ "All", "namespacemembers.html", null ],
        [ "Functions/Subroutines", "namespacemembers_func.html", null ],
        [ "Variables", "namespacemembers_vars.html", null ]
      ] ]
    ] ],
    [ "Files", null, [
      [ "File List", "files.html", "files" ],
      [ "File Members", "globals.html", [
        [ "All", "globals.html", null ],
        [ "Functions/Subroutines", "globals_func.html", null ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"dct_8F90.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';