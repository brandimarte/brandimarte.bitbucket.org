var searchData=
[
  ['dct',['dct',['../namespacedct.html',1,'']]],
  ['dct_2ef90',['dct.F90',['../dct_8F90.html',1,'']]],
  ['dctdct',['dctdct',['../namespacedct.html#a48abefeb6e46accc9daa9e496e4544b2',1,'dct']]],
  ['dctfree',['dctfree',['../namespacedct.html#af215aa8817afef86cd8c7879572a53e5',1,'dct']]],
  ['dctgrid',['dctgrid',['../namespacedct.html#a0e817791abf5665f0f77cc1d4f19e175',1,'dct']]],
  ['dctnaive',['dctnaive',['../namespacedct.html#ae50d35a9eadd9973c195c67bad561bbe',1,'dct']]],
  ['delta',['delta',['../namespaceoptions.html#af00a38abdfd6a4e5a77837793771c687',1,'options']]],
  ['dp',['dp',['../namespaceprecision.html#a94afaea97d26b8c1d0eeca7cd4d00c1a',1,'precision']]],
  ['dstart',['dstart',['../namespaceoptions.html#a5d69957c86c0df312276583a3932d2f5',1,'options']]],
  ['dw',['dw',['../namespaceoptions.html#a12d0a1a9ef81defb54b0287ac1099964',1,'options']]]
];
